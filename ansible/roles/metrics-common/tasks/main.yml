---
- name: consistent environment for tor metrics development
  block:
  - name: install dependency packages
    apt:
      pkg: "{{ metrics_dependency_pkgs }}"
      state: latest
      update_cache: yes
  - name: install dependency (backport) packages
    apt:
      pkg: "{{ metrics_backport_pkgs }}"
      state: latest
      update_cache: yes
      default_release: "{{ ansible_distribution_release }}-backports"
  - name: format additional volumes
    filesystem:
      fstype: "{{ item.fstype }}"
      device: "{{ item.device }}"
    with_items: "{{ metrics_additional_volumes }}"
  - name: add additional volumes to fstab
    mount:
      src: "{{ item.device }}"
      path: "{{ item.mountpoint }}"
      fstype: "{{ item.fstype }}"
      dump: "0"
      passno: "2"
      state: mounted
    with_items: "{{ metrics_additional_volumes }}"
  - name: set timezone to UTC
    timezone:
      name: UTC
  - name: enable password-less sudo for sudo group
    lineinfile:
      path: /etc/sudoers
      regexp: '^%sudo'
      line: '%sudo ALL=(ALL) NOPASSWD: ALL'
      validate: 'visudo -cf %s'
  - name: create metrics users
    user:
      name: "{{ item }}"
      password: "*"
    with_items: "{{ metrics_users }}"
  - name: ensure users are in correct primary group and sudo group
    user:
      name: "{{ item }}"
      group: "{{ item }}"
      append: yes
      groups: "sudo"
    with_items: "{{ metrics_users }}"
  - name: disable root password
    user:
      name: root
      password: '*'
  - name: set up authorized keys
    authorized_key:
      user: "{{ item }}"
      state: present
      exclusive: yes
      key: "{{ lookup('file', 'ssh_user_keys/' + item) }}"
    with_items: "{{ metrics_users }}"
  - name: sshd PermitRootLogin=no
    lineinfile:
      dest: "/etc/ssh/sshd_config"
      regexp: "^#?PermitRootLogin"
      line: "PermitRootLogin prohibit-password"
      state: present
    notify: "reload sshd"
  - name: sshd PasswordAuthentication=no
    lineinfile:
      dest: "/etc/ssh/sshd_config"
      regexp: "^#?PasswordAuthentication"
      line: "PasswordAuthentication no"
      state: present
    notify: "reload sshd"
  - name: install vim defaults
    become: true
    when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'
    copy:
      src: vimrc.local
      dest: /etc/vim/vimrc.local
  - name: add backports repository
    apt_repository:
      repo: 'deb http://http.debian.net/debian {{ ansible_distribution_release }}-backports main contrib non-free'
      state: present
  - name: link home directories /home
    file:
      src: "{{ item.real_home }}"
      dest: "/home/{{ item.name }}"
      state: link
      force: yes
      follow: no
    with_items: "{{ metrics_service_users }}"
  - name: create service users
    user:
      name: "{{ item.name }}"
      comment: "{{ item.comment }}"
      uid: "{{ item.uid }}"
      state: present
      create_home: no
    with_items: "{{ metrics_service_users }}"
  - name: create service directories
    file:
      path: "/srv/{{ item.name }}"
      state: directory
      owner: "{{ item.owner }}"
      group: "{{ item.group }}"
      mode: 0755
    with_items: "{{ metrics_service_directories }}"
  - name: create real service user home directories
    file:
      path: "{{ item.real_home }}"
      state: directory
      owner: "{{ item.name }}"
      group: "{{ item.name }}"
      mode: 0755
    with_items: "{{ metrics_service_users }}"
  - name: enable lingering for service users
    shell: "loginctl enable-linger {{ item.name }}"
    with_items: "{{ metrics_service_users }}"
  become: yes
